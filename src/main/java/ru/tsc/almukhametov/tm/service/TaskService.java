package ru.tsc.almukhametov.tm.service;

import ru.tsc.almukhametov.tm.api.repository.ITaskRepository;
import ru.tsc.almukhametov.tm.api.service.ITaskService;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public void add(Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    @Override
    public void remove(Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findById(id);
    }

    @Override
    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findByName(name);
    }

    @Override
    public Task findByIndex(Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task removeById(String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.removeByName(name);
    }

    @Override
    public Task removeByIndex(Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = taskRepository.findById(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = taskRepository.findByIndex(index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public boolean existById(final String id) {
        if (id == null || id.isEmpty()) return false;
        return taskRepository.existById(id);
    }

    @Override
    public boolean existByIndex(final int index) {
        return taskRepository.existByIndex(index);
    }

    @Override
    public Task startById(final String id) {
        if (id == null || id.isEmpty()) return null;
        final Task task = taskRepository.findById(id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        final Task task = taskRepository.findByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        final Task task = taskRepository.findByName(name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishById(final String id) {
        if (id == null || id.isEmpty()) return null;
        final Task task = taskRepository.findById(id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        final Task task = taskRepository.findByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        final Task task = taskRepository.findById(name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) return null;
        if (status == null) return null;
        return taskRepository.changeTaskStatusById(id, status);
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0) return null;
        if (status == null) return null;
        return taskRepository.changeTaskStatusByIndex(index, status);
    }

    @Override
    public Task changeTaskStatusByName(final String name, final Status status) {
        if (name == null || name.isEmpty()) return null;
        if (status == null) return null;
        return taskRepository.changeTaskStatusByName(name, status);
    }

}
