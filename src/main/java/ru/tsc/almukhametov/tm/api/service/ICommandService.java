package ru.tsc.almukhametov.tm.api.service;

import ru.tsc.almukhametov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
