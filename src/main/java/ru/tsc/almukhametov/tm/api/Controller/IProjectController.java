package ru.tsc.almukhametov.tm.api.Controller;

import ru.tsc.almukhametov.tm.model.Project;

import java.util.List;

public interface IProjectController {

    void clearProjects();

    void createProjects();

    void showList();

    void showById();

    void showByIndex();

    void showByName();

    void removeById();

    void removeByIndex();

    void removeByName();

    void updateById();

    void updateByIndex();

    void startById();

    void startByIndex();

    void startByName();

    void finishById();

    void finishByIndex();

    void finishByName();

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

    void changeProjectStatusByName();

}
