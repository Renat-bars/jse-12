package ru.tsc.almukhametov.tm.api.Controller;

public interface ICommandController {

    void showInfo();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

    void showErrArg();

    void showErrCommand();

    void exit();

}
